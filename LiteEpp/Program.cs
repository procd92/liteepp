﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using LiteServer;

namespace LiteEpp
{
    class Program
    {
        static void Main(string[] args)
        {
            var server = new LiteServer.LiteServer(IPAddress.Any, 8080);
            var connected = 0;
            
            server.OnConnect += (sender, eventArgs) =>
            {
                ++connected;
                Console.Clear();
                Console.WriteLine($"Connected Clients: {connected.ToString()}");
            };

            server.OnMessage += (sender, eventArgs) =>
            {
                var mem = eventArgs.Message as MemoryStream;
                
                var b = new byte[1024 * 1024 * 10];
                var rr = mem.Read(b, 0, b.Length);
                eventArgs.Connection.Stream.Write(b, 0, rr);
            };

//            server.OnException += (sender, eventArgs) =>
//            {
//                Console.WriteLine($"Exception: {eventArgs.ExceptionObject.ToString()}");
//            };
//
//            server.OnClose += (sender, eventArgs) =>
//            {
//                Console.WriteLine($"Client connection closed.");
//            }; 
            
            var task = server.RunAsync();
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();
            Console.ReadKey();
        }
    }
}