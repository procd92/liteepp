﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using LiteServer.Extensions;

namespace LiteServer
{
    // TODO: Implement IDisposable interface
    public class LiteServer
    {
        public event ConnectionEventHandler OnConnect;
        public event MessageEventHandler OnMessage;
        public event UnhandledExceptionEventHandler OnException;
        public event CloseEventHandler OnClose;

        private readonly TcpListener _server;
        private readonly List<Connection> _connectedClients;
        private readonly byte[] _testBuffer;
        private readonly object _connectionsListLock;
        
        private bool _serverStarted;
        private CancellationTokenSource _cancellationTokenSource;

        public List<IMessageDecoder> DecodePipeline { get; set; }
        public List<IMessageEncoder> EncodePipeline { get; set; }
        
        public LiteServer(IPAddress ip, int port)
        {
            _server = new TcpListener(ip, port);
            _serverStarted = false;
            _connectedClients = new List<Connection>();
            _testBuffer = new byte[1];
            _connectionsListLock = new object();
        }

        public Task RunAsync()
        {
            if (_serverStarted)
            {
                throw new InvalidOperationException("Server is already running");
            }

            _cancellationTokenSource = new CancellationTokenSource();
            var task = Task.Run(Run, _cancellationTokenSource.Token);
            task.ContinueWith(ServerTaskCanceled, TaskContinuationOptions.OnlyOnCanceled);
            _serverStarted = true;
            
            return task;
        }

        private async Task Run()
        {
            var cancellationToken = _cancellationTokenSource.Token;
            
            _server.Start();
            while (true)
            {
                if (cancellationToken.IsCancellationRequested) break;
                
                while (_server.Pending()) await AcceptClientAsync();

                CleanDirtyConnections();
                // Delay 100 ms to avoid CPU overload
                await Task.Delay(Common.EventLoopDelayMs, cancellationToken);
            }

            CleanUpServer();
        }

        private void CleanDirtyConnections()
        {
            lock (_connectionsListLock)
            {
                var dirtyConnections = _connectedClients
                    .Where(c => c.Task.Status == TaskStatus.RanToCompletion ||
                                c.Task.Status == TaskStatus.Faulted ||
                                c.Task.Status == TaskStatus.Canceled)
                    .ToList();

                dirtyConnections.ForEach(c =>
                {
                    Console.WriteLine($"Dirty Connection: {c.TcpClient.Client.RemoteEndPoint}");
                    if (c.Task.Exception != null)
                    {
                        Console.WriteLine($"Has Exception: {c.Task.Exception}");
                    }

                    c.Close();

                    _connectedClients.Remove(c);
                });
            }
        }

        private async Task AcceptClientAsync()
        {
            var client = await _server.AcceptTcpClientAsync();
            
            if (client.GetConnectionStatus(_testBuffer) && client.Connected)
            {
                var connection = new Connection(client);

                lock (_connectionsListLock)
                {
                    _connectedClients.Add(connection);
                }

                connection.OnMessage += ConnectionOnMessage;
                connection.OnClose += ConnectionOnClose;
                connection.OnException += ConnectionOnException;
                OnConnect?.Invoke(this, new ConnectionEventArgs(connection));
            
                connection.StartListening(_cancellationTokenSource.Token);
            }
            else
            {
                client.Close();
                client.Dispose();
            }
        }

        private void ConnectionOnMessage(object sender, MessageEventArgs e)
        {
            OnMessage?.Invoke(sender, e);
        }
        
        private void ConnectionOnException(object sender, UnhandledExceptionEventArgs e)
        {
            OnException?.Invoke(sender, e);
        }
        
        private void ConnectionOnClose(object sender, EventArgs e)
        {
            OnClose?.Invoke(sender, e);
            
            // Clean up client
            lock (_connectionsListLock)
            {
                _connectedClients.Remove(sender as Connection);
            }
        }

        public void Stop()
        {
            _cancellationTokenSource.Cancel();
        }

        private void ServerTaskCanceled(Task serverTask)
        {
            _serverStarted = false;
        }

        private void CleanUpServer()
        {
            // TODO: Close and remove all connections
        }
    }
}