using System;
using System.Collections.Generic;
using System.IO;

namespace LiteServer
{
    public delegate void ConnectionEventHandler(object sender, ConnectionEventArgs e);
    public delegate void MessageEventHandler(object sender, MessageEventArgs e);
    public delegate void CloseEventHandler(object sender, EventArgs e);

    internal static class Common
    {
        internal const int BufferSize = 1024 * 1024 * 10;
        internal const int EventLoopDelayMs = 100;
        internal const int StreamTimeoutMs = 30000;
        internal static bool PersistConnection = false;
    }

    public class ConnectionEventArgs : EventArgs
    {
        public Connection Connection { get; }

        public ConnectionEventArgs(Connection connection)
        {
            Connection = connection;
        }
    }

    public class MessageEventArgs : EventArgs
    {
        public Connection Connection { get; }
        public object Message { get; }

        public MessageEventArgs(Connection connection, object message)
        {
            Connection = connection;
            Message = message;
        }
    }
}