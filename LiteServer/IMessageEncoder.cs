namespace LiteServer
{
    public interface IMessageEncoder
    {
        object Encode(object input);
    }
}