using System.Net.Sockets;

namespace LiteServer.Extensions
{
    public static class TcpClientExtensions
    {
        // Taken from here: https://social.msdn.microsoft.com/Forums/en-US/c857cad5-2eb6-4b6c-b0b5-7f4ce320c5cd/c-how-to-determine-if-a-tcpclient-has-been-disconnected?forum=netfxnetcom
        public static bool GetConnectionStatus(this TcpClient client, byte[] buffer)
        {
            if (!client.Client.Poll(0, SelectMode.SelectRead)) return true;
            return client.Client.Receive(buffer, SocketFlags.Peek) != 0;
            
//            if (client.Client.Connected)
//            {
//                if ((client.Client.Poll(0, SelectMode.SelectWrite)) && (!client.Client.Poll(0, SelectMode.SelectError)))
//                {
//                    if (client.Client.Receive(buffer, SocketFlags.Peek) == 0)
//                    {
//                        return false;
//                    }
//                    else
//                    {
//                        return true;
//                    }
//                }
//                else
//                {
//                    return false;
//                }
//            }
//            else
//            {
//                return false;
//            }
        }
    }
}