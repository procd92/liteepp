using System;
using System.IO;
using System.Net.Sockets;

namespace LiteServer.Extensions
{
    internal static class ExceptionExtensions
    {
        internal static bool IsSocketTimeout(this Exception e)
        {
            return e is IOException ioe && ioe.InnerException is SocketException se &&
                   se.SocketErrorCode == SocketError.TimedOut;
        }
        
        internal static bool IsConnectionReset(this Exception e)
        {
            return e is IOException ioe && ioe.InnerException is SocketException se &&
                   se.SocketErrorCode == SocketError.ConnectionReset;
        }
    }
}