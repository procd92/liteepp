using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using LiteServer.Extensions;

namespace LiteServer
{
    public class Connection
    {   
        public event MessageEventHandler OnMessage;
        public event UnhandledExceptionEventHandler OnException;
        public event CloseEventHandler OnClose;

        private readonly TcpClient _client;
        private readonly List<IMessageDecoder> _decodePipeline;
        private readonly List<IMessageEncoder> _encodePipeline;
        private readonly NetworkStream _stream;
        
        private CancellationToken _cancellationToken;
        
        private byte[] _inboundBuffer;
        private byte[] _outboundBuffer;
        private int _inboundBufferLength;
        private int _outboundBufferLength;
        private bool _active;

        public Task Task { get; private set; }
        public TcpClient TcpClient => _client;
        public NetworkStream Stream => _stream;

        public Connection(TcpClient client, List<IMessageDecoder> decodePipeline = null,
            List<IMessageEncoder> encodePipeline = null)
        {
            _client = client;
            _decodePipeline = decodePipeline;
            _encodePipeline = encodePipeline;
            _stream = client.GetStream();
            _stream.ReadTimeout = Common.StreamTimeoutMs;
            _stream.WriteTimeout = Common.StreamTimeoutMs;
            _inboundBuffer = new byte[Common.BufferSize];
            _inboundBufferLength = 0;
            _outboundBuffer = new byte[Common.BufferSize];
            _outboundBufferLength = 0;
            _active = false;
        }

        public void StartListening(CancellationToken cancellationToken)
        {
            _cancellationToken = cancellationToken;
            _active = true;
            Task = Task.Run(Run, _cancellationToken);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="count"></param>
        /// <returns></returns>
        /// <exception cref="InternalBufferOverflowException">Thrown when the internal outbound buffer is full</exception>
        public void Write(byte[] buffer, int offset, int count)
        {
            if (count > (_outboundBuffer.Length - _outboundBufferLength))
            {
                throw new InternalBufferOverflowException("Outbound buffer is full");
            }

            var finalPosition = offset + count;
            for (var position = offset; position < finalPosition; ++position)
            {
                _outboundBuffer[_outboundBufferLength++] = buffer[position];
            }
        }

        public void Close()
        {
            OnClose?.Invoke(this, EventArgs.Empty);
            _client.Client.Shutdown(SocketShutdown.Both);
            _client.Client.Disconnect(false);
            _client.Close();
            _client.Dispose();
        }

        private async Task Run()
        {
            var connectionTestBuffer = new byte[1];

            try
            {
                while (_active)
                {
                    if (_cancellationToken.IsCancellationRequested) break;

                    await ReadInBuffer();
                    await ProcessInBuffer();
                    await WriteOutBuffer();
                    
                    if (!_client.GetConnectionStatus(connectionTestBuffer)) break;
                }
            }
            catch (Exception e)
            {
                // Handle unhandled exceptions
                await HandleException(e);
            }
            
            Close();
        }

        private async Task ReadInBuffer()
        {
            if (!_active) return;
            if (_inboundBufferLength == _inboundBuffer.Length) return;
            var bytesToRead = _inboundBuffer.Length - _inboundBufferLength;
            if (bytesToRead == 0) return;

            try
            {
                _inboundBufferLength += _stream.Read(_inboundBuffer, _inboundBufferLength, 
                    bytesToRead);
            }
            catch (Exception e)
            {
                if (Common.PersistConnection && e.IsSocketTimeout())
                {
                    return;
                }
                
                await HandleException(e);
            }
        }

        private async Task WriteOutBuffer()
        {
            if (!_active) return;
            if (_outboundBufferLength == 0) return;
            
            try
            {
                await _stream.WriteAsync(_outboundBuffer, 0, _outboundBufferLength, _cancellationToken);
                _outboundBufferLength = 0;
            }
            catch (Exception e)
            {
                await HandleException(e);
            }
        }

        private async Task ProcessInBuffer()
        {
            if (!_active) return;
            while (_inboundBufferLength > 0)
            {
                var memoryStream = new MemoryStream(_inboundBuffer, 0, _inboundBufferLength, false);
                OnMessage?.Invoke(this, new MessageEventArgs(this, memoryStream));
                
                var streamPos = memoryStream.Position;
                if (streamPos > 0)
                {
                    // Shift all remaining bytes to left and update buffer length
                    var newLength = _inboundBufferLength - streamPos;
                    for (var position = streamPos; position < _inboundBufferLength; ++position)
                    {
                        _inboundBuffer[position - streamPos] = _inboundBuffer[position];
                    }

                    _inboundBufferLength = (int)newLength;
                }
                else
                {
                    break;
                }
            }
        }

        private async Task HandleException(Exception exception)
        {
            // TODO: Log Exception

            // Handle Timeouts, close connection
            if (exception.IsSocketTimeout() || exception.IsConnectionReset())
            {
                _active = false;
            }
            else
            {
                Console.WriteLine(exception);
            }
            
            OnException?.Invoke(this, new UnhandledExceptionEventArgs(exception, false));
        }
    }
}