using System.Collections.Generic;

namespace LiteServer
{
    public interface IMessageDecoder
    {
        List<object> Decode(object input);
    }
}